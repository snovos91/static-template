var express = require('express');
var http = require('http');
var fs = require('fs');
var app = express();
var bodyParser = require('body-parser');
var _ = require('underscore');
var port = process.env.PORT || 8080;

app.use( bodyParser.json() );

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(express.static('./'));

app.get('/', function(req,res) {
    res.sendFile('index.html');
});

app.get('/content', function(req,res) {
    var tmpl = '';
    var options = {
        host: 'www.lofty.com',
        method: 'GET',
        path: '/about-lofty',
        headers: {
            'Content-type' : 'text/html'
        }
    };

    callback = function(response) {
        var data = '';
        var bodyHtml = '';
        response.on('data', function (chunk) {
            data += chunk;
        });

        response.on('end', function () {
            bodyHtml = /<body.*?>([\s\S]*)<\/body>/.exec(data)[1];
            var file = __dirname + '/app/src/templates/page.html';
            fs.readFile( file, function (err, data) {
                if (err) {
                    throw err;
                }
                var re = /<%([^%>]+)?%>/g;
                fs.writeFile(file, data.toString().replace(re, bodyHtml));

            });
            res.status(200).json({'success': true});
        });
    };

    http.request(options, callback).end();

});

var server = app.listen(port, function () {

    var host = server.address().address;

    console.log('Application starts at http://%s:%s', host, port);

});
