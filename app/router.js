define([
  'jquery',
  'underscore',
  'backbone',
  'marionette',
  'src/controllers/appCtrl'
],

function ($, _, Backbone, Marionette, AppCtrl) {

  'use strict';

  return Backbone.Router.extend({

    routes : {
      '' : 'getContent'
    },


    initialize : function () {
      this.initControllers();
    },


    initControllers : function () {
      this.appCtrl = new AppCtrl();
    },


    getContent : function () {
      this.appCtrl.getContent();
    }

  });

});

