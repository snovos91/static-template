define([
  'jquery',
  'underscore',
  'backbone',
  'marionette'
],

function ($, _, Backbone, Marionette) {

  'use strict';

  return Marionette.Layout.extend({
    el : '[data-main-layout]',
    template : 'layout',
    regions : {
      mainRegion : '[data-main-region]'
    }
  });

});
