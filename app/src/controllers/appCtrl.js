define([
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'src/views/layout',
    'src/models/appModel',
    'src/views/sampleView'
],

function ($, _, Backbone, Marionette, Layout, Model, SampleView) {

    'use strict';

    return Marionette.Controller.extend({

        initialize : function () {
            this.initComponents();
        },


        initComponents : function () {
            this.layout = new Layout();
            this.layout.render();
            this.model = new Model();
            this.mainView = new SampleView();
        },


        getContent : function () {
            this.model.fetch({context:this}).done(function() {
                console.log(this.layout);
                this.layout.mainRegion.show(this.mainView)
            });
        }
    });

});
